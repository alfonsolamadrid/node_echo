var express = require('express'),
    bodyParser = require('body-parser');

require('body-parser-xml')(bodyParser);

var app = express();

app.use(bodyParser.xml({
  limit: '1MB',   			 // Reject payload bigger than 1 MB
  xmlParseOptions: {
    normalize: true,     // Trim whitespace inside text nodes
    normalizeTags: true, // Transform tags to lowercase
    explicitArray: false // Only put nodes in array if >1
  }
}));

app.post('/', function(req, res, body) {
  // Any request with an XML payload will be parsed
  // and a JavaScript object produced on req.body
  // corresponding to the request payload.
  console.log(req.body);
  res.status(200).end();
});

app.listen(3333, function(){
  console.log('Corriendo en puerto 3333');
});

